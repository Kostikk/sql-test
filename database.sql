/* оздание таблиц 
	1 табл - id  и юзернейм
	2 табл - id, внешний ключ на лайкнувшего, внешний влюч на того кого лайкнули
	запись с лайками удаляеться если удалить пользователя
	*/


CREATE TABLE `sys`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(40) NOT NULL,
  PRIMARY KEY (`is`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE);


CREATE TABLE `sys`.`likes` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `like_from` INT NOT NULL,
  `like_to` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `like_to_idx` (`like_to` ASC) VISIBLE,
  INDEX `like_from_idx` (`like_from` ASC) VISIBLE,
  CONSTRAINT `like_to`
    FOREIGN KEY (`like_to`)
    REFERENCES `sys`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `like_from`
    FOREIGN KEY (`like_from`)
    REFERENCES `sys`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION);

/*заполнение пользователями*/

INSERT INTO `sys`.`users` (`id`, `username`) VALUES ('1', 'userA');
INSERT INTO `sys`.`users` (`id`, `username`) VALUES ('2', 'userB');
INSERT INTO `sys`.`users` (`id`, `username`) VALUES ('3', 'userC');
INSERT INTO `sys`.`users` (`id`, `username`) VALUES ('4', 'blabla');
INSERT INTO `sys`.`users` (`id`, `username`) VALUES ('5', 'kek');
INSERT INTO `sys`.`users` (`id`, `username`) VALUES ('6', 'userD');
INSERT INTO `sys`.`users` (`id`, `username`) VALUES ('7', 'test');
INSERT INTO `sys`.`users` (`id`, `username`) VALUES ('8', 'user');

/*генерация таблицы лайками лайками */

DELIMITER $$
DROP PROCEDURE genereteTestData$$
CREATE PROCEDURE sys.genereteTestData()
BEGIN

DECLARE maximal int;
DECLARE i int ;
SET maximal = 30;
SET i = 0;


  WHILE i < maximal DO
    INSERT INTO sys.likes ('id', 'like_from', 'like_to') VALUES (i, i, i );
    SET i = i + 1;
  END WHILE;

END

CALL genereteTestData();
DELIMITER ;

SELECT * FROM sys.likes;
